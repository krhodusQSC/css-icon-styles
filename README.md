# Q-SYS Example Icon Font Style Sheets #

These examples show how to incorporate the Material and Foundation icon libraries into your CSS Style sheets. 

### Requirements ###

* Q-SYS Designer 9.2

### How do I get set up? ###

* Download a copy of the entire repo
* Copy "QSC_Icons_Foundation", "QSC_Icons_Material", and "MyProjectStyle" to "C:\Users\Little Billy\Documents\QSC\Q-Sys Designer\Styles"
* Use Style Manager in Q-SYS Designer to add each style to your Designer file
* Set the UCI's Style Sheet to "MyProjectStyle"
* Modify MyProjectStyle as necessary for the project
* Assign icon CSS class to buttons that desire to have an icon be placed on them. 

### Note: This example has close to 2,000 icon classes defined in it. Before using in a customer project, it is highly advised to trim down the available icons to a more managable subset of the icons available. ###


### License ###
* MIT